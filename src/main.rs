use anyhow::{Context, Result};
use chrono::{DateTime, Local};
use mailparse::*;
use newline_converter::unix2dos;
use regex::{Captures, Regex};
use std::io::{self, Read, Write};
use std::process;

fn write_file(contents: &str) -> Result<()> {
    io::stdout()
        .write_all(contents.as_bytes())
        .context("Can't write to stdout")?;

    Ok(())
}

fn main() -> Result<()> {
    let mut contents = String::new();
    io::stdin()
        .read_to_string(&mut contents)
        .context("Can't read stdin")?;

    let contents_dos = unix2dos(&contents);

    let email = parse_mail(contents_dos.as_bytes()).context("Can't parse email")?;

    let time: &str = &email
        .headers
        .get_first_value("Date")
        .context("Missing Date header")?;
    let time = &time.replace("Date: ", "");
    let time = time.trim();

    // Remove ending (CST) endings
    let re_date = Regex::new(r" *\([A-Z]*\)").unwrap();
    let time = re_date.replace(time, "");

    let local_time = DateTime::parse_from_rfc2822(&time).unwrap_or_else(|_| {
        write_file(&contents).unwrap();
        process::exit(0);
    });

    let local_time = &local_time.with_timezone(&Local);
    let local_time: &str = &local_time.to_rfc2822();
    let local_time_string = "X-Date: ".to_owned() + local_time;

    let re = Regex::new(r"(?m)^(Date:.*)$").unwrap();

    let contents = re.replace(&contents, |caps: &Captures| {
        format!("{}\n{}", &caps[1], local_time_string)
    });

    write_file(&contents)?;

    Ok(())
}
